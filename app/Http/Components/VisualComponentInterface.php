<?php

namespace App\Http\Components;

interface VisualComponentInterface
{
    public function render(): string;
}
