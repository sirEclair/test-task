<?php

namespace App\Http\Components;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class DataGrid implements VisualComponentInterface
{
    protected $controller;
    protected $model;
    protected $fields;
    protected $maxWidth = 12;
    protected $maxFields = 5;

    /**
     * __construct
     *
     * @param  Illuminate\Database\Eloquent\Model $model
     * @param  array $fields of model to render
     * @param  string $controller resource controller with base CRUD actions
     *
     * @return void
     */
    public function __construct(Model $model, array $fields, string $controller)
    {
        $this->model = $model;
        $this->controller = $controller;

        if (count($fields) > $this->maxFields) {
            $fields = array_splice($fields, 0, $this->maxFields);
        }

        $this->fields = $fields;
    }


    protected function getColumnWidth()
    {
        $count = count($this->fields) + 1; // 1 column for action links
        return round(100 / $count, 2);
    }

    /**
     * Get filter parameters from query string
     *
     * @return array
     */
    protected function getFilterParams(): array
    {
        $params = [];

        foreach ($this->fields as $field) {
            $value = request()->query($field, '');

            if ($value) {
                $params[$field] = $value;
            }
        }

        return $params;
    }

    /**
     * Apply 'where' conditions to builder
     *
     * @param  Illuminate\Database\Eloquent\Builder $builder
     * @param  array $params
     *
     * @return Illuminate\Database\Eloquent\Builder
     */
    protected function applyFilterConditions(Builder $builder, array $params): Builder
    {
        foreach ($params as $field => $value) {
            $builder->where($field, 'like', "%$value%");
        }

        return $builder;
    }

    public function render(): string
    {
        $params = $this->getFilterParams();

        $items = $this->applyFilterConditions($this->model->on(), $params);
        $items = $items->paginate()->appends($params);

        return view('components.datagrid', [
            'data' => $items,
            'fields' => $this->fields,
            'controller' => $this->controller,
            'width' => $this->getColumnWidth(),
        ]);
    }
}
