<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Page;

class PageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    
    /**
     * Load page model if exists
     *
     * @param  mixed $id
     *
     * @return App\Models\Page
     * 
     * @throws HttpResponseException if model not found
     */
    protected function getPageModel($id): Page
    {
        $page = Page::find($id);

        if ($page === null)
            abort(404);

        return $page;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.pages');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.page-edit', ['page' => new Page, 'action' => 'store']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $page = new Page();

        $success = $page->fill([
            'title' => $request->input('title'),
            'header' => $request->input('header'),
            'main_content' => $request->input('main_content'),
            'additional_content' => $request->input('additional_content'),
        ])->save();

        if (!$success) {
            // we need to react this
            throw new \Exception('Create operation has been failed');
        }

        return redirect()->action('PageController@show', ['id' => $page->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $page = $this->getPageModel($id);

        return view('backend.page-view', ['page' => $page]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = $this->getPageModel($id);

        return view('backend.page-edit', ['page' => $page, 'action' => 'update']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * 
     * @throws \Exception if Model->save() method returns false
     */
    public function update(Request $request, $id)
    {
        $page = $this->getPageModel($id);

        $success = $page->fill([
            'title' => $request->input('title'),
            'header' => $request->input('header'),
            'main_content' => $request->input('main_content'),
            'additional_content' => $request->input('additional_content'),
        ])->save();

        if (!$success) {
            // we need to react this
            throw new \Exception('Save operation has been failed');
        }

        return redirect()->action('PageController@show', ['id' => $id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Page::destroy($id);
        return back();
    }
}
