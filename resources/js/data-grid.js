{
    let deleteButtons = document.querySelectorAll('.js-delete-item');

    deleteButtons.forEach((item, idx) => {
        item.addEventListener('click', (e) => {
            e.preventDefault();
            
            let form = document.forms.action_form;

            if (confirm('Remove item?')) {
                form._method.value = 'DELETE';
                form.setAttribute(
                    'action',
                    item.getAttribute('href')
                );

                form.submit();
            }
        });
    });
}
