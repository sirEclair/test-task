@php
    $style = "flex-basis:auto; width:{$width}%;";
@endphp

<div class="data-grid">

    <form method="get" action="" class="data-grid__form">
        <div class="row data-grid__row">
                @foreach ($fields as $field)
                    <div class="col data-grid__col" style="{{ $style }}">
                        <input
                            type="text"
                            name="{{ $field }}"
                            placeholder="{{ $field }}"
                            value="{{ request($field, '') }}"
                            class="data-grid__input"
                            title="Click to type search phrase"
                        />
                    </div>
                @endforeach

                <div class="col data-grid__col" style="{{ $style }}">
                    <input type="submit" value="Search" class="btn btn-primary" />
                    <a href="{{ action([$controller, 'index']) }}" title="Clear search params">X</a>
                </div>
        </div>

        @forelse($data as $item)
            <div class="row data-grid__row">
                @foreach($fields as $field)
                <div class="col data-grid__col" style="{{ $style }}">{{ $item->$field }}</div>
                @endforeach

                <div  class="col data-grid__col" style="{{ $style }}">
                    <a href="{{ action([$controller, 'edit'], ['id' => $item->id]) }}" title="Edit">E</a> |
                    <a href="{{ action([$controller, 'show'], ['id' => $item->id]) }}" title="Show">S</a> |
                    <a href="{{ action([$controller, 'destroy'], ['id' => $item->id]) }}" class="js-delete-item" title="Delete">D</a>
                </div>
            </div>
        @empty
            <div class="row justify-content-center">
                <div class="col-3">Not found</div>
            </div>
        @endforelse

        <div class="row justify-content-end">
            <a href="{{ action([$controller, 'create']) }}" title="Create new item" class="btn btn-primary">Create</a>
        </div>


        {{ $data->links() }}
    </form>

    <form name="action_form" action="" method="POST" style="display:none;">
        @csrf
        @method('POST')
    </form>
</div>
