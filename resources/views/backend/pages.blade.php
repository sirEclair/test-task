@extends('layouts.app')

@section('content')
    @datagrid(
        new \App\Models\Page,
        ['id', 'title', 'updated_at'],
        \App\Http\Controllers\PageController::class
    )
@endsection
