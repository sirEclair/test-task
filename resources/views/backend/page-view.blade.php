@extends('layouts.app')

{{ config(['app.name' => $page->title]) }}

@section('content')
    <div class="page">
        <h1>{{ $page->header }}</h1>
        <div class="page__comment">Created: {{ $page->created_at }}. Last update: {{ $page->updated_at }}</div>

        <div class="page_main-content">{{ $page->main_content }}</div>
        <div class="page__additional-content">{{ $page->additional_content }}</div>

        <div class="row">
            <div class="col-sm-6 offset-sm-6">
                <a href="{{ back()->getTargetUrl() }}" class="btn float-right">Back</a>
                <a href="{{ action('PageController@edit', ['id' => $page->id]) }}" class="btn btn-primary float-right">Edit</a>
            </div>
        </div>
    </div>
@endsection
