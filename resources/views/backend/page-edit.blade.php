@extends('layouts.app')

@section('content')
    <form method="post" action="{{ action('PageController@'. $action, ['id' => request('page')]) }}">
        @if ($action == 'update')
            @method('PUT')
        @endif

        @csrf

        <div class="form-group">
            <label>
                Title: <input class="form-control" type="text" name="title" maxlength="191" value="{{ $page->title }}" />
            </label>
        </div>

        <div class="form-group">
            <label>
                Header: <input class="form-control" type="text" name="header" maxlength="191" value="{{ $page->header }}" />
            </label>
        </div>

        <div class="form-group">
            <label for="main_content">Main content:</label>
            <textarea class="form-control" id="main_content" name="main_content">{{ $page->main_content }}</textarea>
        </div>

        <div class="form-group">
            <label for="additional_content">Additional content:</label>
            <textarea class="form-control" id="additional_content" name="additional_content">{{ $page->additional_content }}</textarea>
        </div>

        <div class="row">
            <div class="col-sm-6 offset-sm-6">
                <a href="{{ back()->getTargetUrl() }}" class="btn float-right">Cancel</a>
                <input type="submit" value="Save" class="btn btn-primary float-right" />
            </div>
        </div>
    </form>
@endsection
